//
//  TTMAppDelegate.h
//  HelloWord
//
//  Created by admin on 9/4/14.
//  Copyright (c) 2014 TriThucMoi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
